\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{sdm}[2015/02/02 v0.1
LaTeX document class for 
Universite Rennes 1]

%extension de article
\LoadClassWithOptions{article}
\RequirePackage{fullpage}
\RequirePackage{graphicx}
\RequirePackage{ifthen}

\def\supervisorOne#1{\gdef\@supervisorOne{#1}}
\def\supervisorTwo#1{\gdef\@supervisorTwo{#1}}
\def\supervisorThree#1{\gdef\@supervisorThree{#1}}
\def\team#1{\gdef\@team{#1}}
\def\school#1{\gdef\@school{#1}}
\def\domain#1{\gdef\@domain{#1}}
\def\abstract#1{\gdef\@abstract{#1}}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\newcommand{\mode}{internship}

\renewcommand{\maketitle}{
\begin{titlepage}

\begin{center}

% Upper part of the page
\makebox[\textwidth]{
\includegraphics[width=\textwidth]{./logo/header}
}
\\[1cm]
\textsc{\Large Master research Internship}
\vspace{1cm}

\includegraphics[height=6mm]{./logo/rennes1} \hspace{2mm}
\includegraphics[height=6mm]{./logo/ens} \hspace{2mm}
\includegraphics[height=6mm]{./logo/insa} \hspace{2mm}
\includegraphics[height=6mm]{./logo/supelec} \hspace{2mm}
\includegraphics[height=6mm]{./logo/esir} \hspace{2mm}
\includegraphics[height=6mm]{./logo/enssat} \hspace{2mm}
\includegraphics[height=6mm]{./logo/ubs} \hspace{2mm}

\vspace{1cm}

%
\textsc{\Large \ifthenelse{\equal{\mode}{biblio}}{Bibliographic}{Internship} report }\\[0.5cm]

% The title of your report
\HRule \\[0.4cm]
{ \Large \bfseries {\@title} }\\[0.4cm]

\HRule \\[1.5cm]
% The domain of your research 
\begin{flushleft}
\textbf{\@domain}
\end{flushleft}


%
% Author and supervisor(s)
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
\@author
\end{flushleft}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisors:} \\
%
% name(s) of your supervisor(s)
\ifthenelse{\equal{\@supervisorOne}{}}{}{\@supervisorOne\\}
\ifthenelse{\equal{\@supervisorTwo}{}}{}{\@supervisorTwo\\}
\ifthenelse{\equal{\@supervisorThree}{}}{}{\@supervisorThree\\}
% Name of your team
\@team
\end{flushright}
\end{minipage}

\vfill


% INCLUDE HERE THE LOGO OF YOUR INSTITUTION
%\textbf{INSERT ``\%'' IN FRONT OF ALL THE LOGO YOU DO NOT NEED - A SINGLE ONE SHOULD REMAIN AT THE BOTTOM OF THIS PAGE}
\begin{flushleft}
%\includegraphics[width=0.09\textwidth]{./logo/\@school}
\includegraphics[height=1cm]{./logo/\@school}
\end{flushleft}
\end{center}
\end{titlepage}

\par\noindent
\textbf{Abstract:} \@abstract

%\newpage

% compile twice to get the table of contents
\tableofcontents
\thispagestyle{empty}
\newpage


\setcounter{page}{1} 
}
