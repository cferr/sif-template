TEX=$(wildcard *.tex)
BIB=$(wildcard *.bib)
BBL=$(BIB:.bib=.bbl)
MAIN=main
AUX=${MAIN}.aux

all: ${MAIN}.pdf

%.aux: %.tex
	pdflatex $(@:.aux=.tex)

%.bbl: %.bib $(AUX)
	bibtex ${MAIN}

${MAIN}.pdf: $(TEX) $(AUX) $(BBL)
	pdflatex ${MAIN}.tex
	pdflatex ${MAIN}.tex

clean:
	rm -f ${MAIN}.pdf *.log *.aux *.bbl *.toc *.blg
